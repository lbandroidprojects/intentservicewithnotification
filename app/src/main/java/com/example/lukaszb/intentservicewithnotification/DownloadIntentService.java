package com.example.lukaszb.intentservicewithnotification;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.os.Environment;
import android.support.v4.app.TaskStackBuilder;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DownloadIntentService extends IntentService {

    private Context activityContext;
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_DOWNLOAD = "com.example.lukaszb.intentservicewithnotification.action.DOWNLOAD";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.example.lukaszb.intentservicewithnotification.action.DOWNLOAD_PARAM";

    public DownloadIntentService() {
        super("DownloadIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionDownload(Context context, String param1) {
        Intent intent = new Intent(context, DownloadIntentService.class);
        intent.setAction(ACTION_DOWNLOAD);
        intent.putExtra(EXTRA_PARAM1, param1);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DOWNLOAD.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);

                handleActionDownload(param1);
            }
        }
        stopSelf();
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionDownload(String param1) {
        final int notificationID=1;
        Notification.Builder builder=new Notification.Builder(this);
        builder.setSmallIcon(android.R.drawable.stat_sys_download);
        builder.setContentTitle("Download started");
        builder.setContentText("Please wait...");
        Intent receiveInt=new Intent(this,ReceiveActivity.class);
//        TaskStackBuilder tsm=TaskStackBuilder.create(this);
//        tsm.addParentStack(ReceiveActivity.class);
//        tsm.addNextIntent(receiveInt);
//        PendingIntent receivePendingIntent=tsm.getPendingIntent(12,PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent receivePendingIntent=PendingIntent.getActivity(this,12,receiveInt,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(receivePendingIntent);
        NotificationManager nm= (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        nm.notify(notificationID,builder.build());
        try {
            URL url = new URL(param1);
            try (BufferedInputStream bis = new BufferedInputStream(url.openStream());
                 FileOutputStream fos=new FileOutputStream(getLocalDir()+"/"+extractFileName(param1))) {
                double fileSize=url.openConnection().getContentLength();
                int bufsize=1024;
                byte[] buffer=new byte[bufsize];
                int count;
                double currentSize=0;
                while((count=bis.read(buffer,0,bufsize))>-1){
                    fos.write(buffer,0,count);
                    currentSize+=count;
                    builder.setProgress(100,(int)(currentSize*100/fileSize),false);
                    nm.notify(notificationID,builder.build());
                }
                builder.setSmallIcon(android.R.drawable.stat_sys_download_done);
                builder.setContentTitle("Download finished");
                builder.setContentText("");
                nm.notify(notificationID,builder.build());

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private String extractFileName(String path){
        return path.substring(path.lastIndexOf("/")+1);
    }

    private String getLocalDir(){
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    }

    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(),"Plik załadowano.",Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}

