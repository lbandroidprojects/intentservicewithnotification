package com.example.lukaszb.intentservicewithnotification;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button b;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b=(Button)findViewById(R.id.button);
        b.setOnClickListener(this);
    }

    boolean isNetworkReady(){
        boolean ready=false;
        ConnectivityManager cm=(ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info=cm.getActiveNetworkInfo();
        if(info!=null)
            ready= info.isConnected() && info.isAvailable();
        return ready;
    }


    @Override
    public void onClick(View view) {
        if(isNetworkReady()){
            DownloadIntentService.startActionDownload(this,"https://cran.r-project.org/doc/manuals/R-intro.pdf");
        }else
        {
            Toast.makeText(this,"Brak połączenia sieciowego",Toast.LENGTH_SHORT).show();}

    }
}
